module awesomeProject

go 1.20

require (
	github.com/blacked/go-zabbix v0.0.0-20170118040903-3c6a95ec4fdc
	github.com/jacobsa/go-serial v0.0.0-20180131005756-15cf729a72d4
)

require golang.org/x/sys v0.9.0 // indirect
