package main

import (
	"flag"
	"fmt"
	. "github.com/blacked/go-zabbix"
	"github.com/jacobsa/go-serial/serial"
	"sync"
	"time"

	"io"
	"log"
	"regexp"
	"strconv"
)

var (
	cacheLock sync.RWMutex
	cache     float64
)

func readValueFromSerial(port io.ReadWriteCloser) (string, error) {

	buffer := make([]byte, 100)

	n, err := port.Read(buffer)

	if err != nil {

		return "", err
	}

	return string(buffer[:n]), nil
}

func extractFloatValueFromString(data string) (float64, error) {

	re := regexp.MustCompile(`[-+]?[0-9]*\.?[0-9]+`)

	match := re.FindString(data)

	if match != "" {

		value, err := strconv.ParseFloat(match, 64)

		if err != nil {

			return 0, err

		}

		return value, nil

	}

	return 0, nil
}

func updateCacheValue(port io.ReadWriteCloser) {
	const tempoDeEsperaInicial = 10 * time.Second // Tempo de espera inicial
	const temperaturaMinima = 10.0
	const temperaturaMaxima = 40.0

	var primeiraLeitura = true
	var _ float64

	// Aguardar o tempo de espera inicial antes de comeÃ§ar a medir
	time.Sleep(tempoDeEsperaInicial)

	for {
		data, err := readValueFromSerial(port)
		if err != nil {
			log.Println("Erro ao ler os dados da porta serial:", err)
			continue
		}

		temperature, err := extractFloatValueFromString(data)
		if err != nil {
			log.Println("Erro ao extrair a temperatura:", err)
			continue
		}

		// Verificar se a temperatura estÃ¡ dentro do intervalo desejado
		if temperature >= temperaturaMinima && temperature <= temperaturaMaxima {
			cacheLock.RLock()
			if primeiraLeitura {
				_ = cache
				primeiraLeitura = false
			}
			cacheLock.RUnlock()

			cacheLock.Lock()
			cache = temperature
			cacheLock.Unlock()
			_ = temperature
		}

		time.Sleep(1500 * time.Millisecond)
	}
}

func logCacheValue() {

	for {

		cacheLock.RLock()

		value := cache

		cacheLock.RUnlock()

		log.Printf("Valor em cache: %f", value)

		time.Sleep(2500 * time.Millisecond)
	}
}

func main() {

	zserver := flag.String("zabbix", "", "zabbix server e.g. '127.0.0.1'")

	targetHost := flag.String("host", "", "zabbix target host e.g. 'myhost'")

	zport := flag.Int("port", 10051, "zabbix server port e.g. 10051")

	serialPort := flag.String("seral_port", "COM5", "valor da porta serial")

	delay := flag.Int("delay", 20, "tempo para envio da metrica para servidor zabbix")

	flag.Parse()

	options := serial.OpenOptions{

		PortName:        *serialPort,
		BaudRate:        9600,
		DataBits:        8,
		StopBits:        1,
		MinimumReadSize: 4,
	}

	if *zserver == "" || *targetHost == "" {
		flag.PrintDefaults()
		return
	}

	fmt.Printf("Connecting to %s:%d to populate trapper items for host %s\n", *zserver, *zport, *targetHost)

	var metrics []*Metric

	port, err := serial.Open(options)

	if err != nil {

		log.Fatal(err)

	}

	defer func(port io.ReadWriteCloser) {

		err := port.Close()

		if err != nil {

			log.Fatal(err)
		}

	}(port)

	go updateCacheValue(port)

	go logCacheValue()

	for {

		var s = fmt.Sprintf("%f", cache)

		metrics = append(metrics, NewMetric(*targetHost, "sala.temperatura", s))

		packet := NewPacket(metrics)

		z := NewSender(*zserver, *zport)

		res, err := z.Send(packet)

		if err != nil {

			fmt.Println("ERROR: ", err)

			return

		}

		fmt.Println(string(res))

		time.Sleep(time.Duration(*delay) * 1000 * time.Millisecond)
	}

}
