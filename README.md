## Projeto de Monitoramento de Temperatura de Datacenter usando Arduino

Este projeto consiste em um sistema de monitoramento de temperatura para um datacenter utilizando o Arduino e o sensor de temperatura LM35, com integra��o ao Zabbix.

### Requisitos

Antes de executar o programa, certifique-se de ter os seguintes requisitos instalados:

    Go: Instalação do Go
    Zabbix: Um servidor Zabbix em execução e configurado

### Componentes Utilizados

- Arduino Uno (ou compat�vel)
- Shield para Arduino - Padawan da RoboCore


### Descri��o do Projeto

O objetivo deste projeto � monitorar a temperatura de um datacenter e enviar os dados coletados para o Zabbix, uma ferramenta de monitoramento de rede. O sensor de temperatura LM35 � conectado ao Arduino por meio do Shield Padawan da RoboCore, que facilita a conex�o de sensores anal�gicos.

O projeto consiste em utilizar o Arduino e o sensor de temperatura LM35 para ler a temperatura do datacenter. Os dados s�o enviados via porta serial para uma aplica��o em Go, que os disponibiliza para o Zabbix . O Zabbix processa e armazena os dados, permitindo o monitoramento da temperatura do datacenter e o recebimento de alertas em caso de varia��es significativas.

### Executando o Programa

Após realizar as configurações necessárias, você pode executar o programa da seguinte maneira:


```sh
go run main.go -zabbix <endereço do servidor Zabbix> -host <host de destino no Zabbix> -port <porta do servidor Zabbix> -seral_port <porta serial> -delay <intervalo em segundos>

```

Certifique-se de substituir <endereço do servidor Zabbix>, <host de destino no Zabbix>, <porta do servidor Zabbix>, <porta serial> e <intervalo em segundos> pelos valores correspondentes.

O programa irá ler a temperatura do sensor conectado à porta serial em intervalos regulares e enviar as métricas para o servidor Zabbix. Os resultados serão exibidos no console.