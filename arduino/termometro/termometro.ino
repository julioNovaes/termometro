const int sensorPin = A0;
float temperatura = 0;

const int intervalo = 500;

const int seletor = A3;

const int Red = 5;
const int Green = 3;
const int Blue = 6;

const int A = A4;
const int B = A5;
const int C = 7;
const int D = 8;
const int E = 5;
const int F = 3;
const int G = 6;

void setup(){

  Serial.begin(9600); 
  
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(A4, OUTPUT);
  pinMode(A5, OUTPUT);
  pinMode(A3, OUTPUT);

  
}

void loop(){

  
    temperatura = (analogRead(sensorPin) * 0.0048828125 * 100);

    Serial.print("Temperatura = ");

    Serial.print(temperatura); 
    
    Serial.println(" C"); 

    if (temperatura > 40.0f)
    {

      digitalWrite(Red,HIGH);

    } else{

      digitalWrite(Green,HIGH);

    }


   delay(2000); 
  
}


void configShield(){


    digitalWrite(seletor,LOW);

    digitalWrite(A, LOW);
    digitalWrite(B, LOW);
    digitalWrite(C, LOW);
    digitalWrite(D, LOW);
    digitalWrite(E, LOW);
    digitalWrite(F, LOW);
    digitalWrite(G, LOW);

    digitalWrite(seletor, HIGH);

}
